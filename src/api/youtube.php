<?php

//
// Init
//

$cURL = curl_init();

//
// Response
//

header("Content-Type: application/json");

echo json_encode(youtube());

//
// Clean up
//

curl_close($cURL);

//
// Functions
//

function get($url) {
  global $cURL;

  curl_setopt($cURL, CURLOPT_URL, $url);
  curl_setopt($cURL, CURLOPT_HTTPHEADER, [
    'Accept: application/json'
  ]);
  curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);

  return json_decode(curl_exec($cURL));
}

function youtube() {

  $env = include('env.php');
  $apiKey = $env['GOOGLE_API_KEY'];

  //
  // Mediya playlist
  //

  $playlistId = 'PL7dWCSr88NFSLYpWHbpFo7jNW1EoxeIpm';
  $playlistUrl = "https://youtube.googleapis.com/youtube/v3/playlistItems?part=snippet%2CcontentDetails&playlistId={$playlistId}&maxResults=50&key={$apiKey}";

  $playlistItems = array_map(function($item) {

    return [
      'category' => 'Youtube',
      'name' => $item->snippet->videoOwnerChannelTitle,
      'description' => $item->snippet->title,
      'type' => 'internal',
      'url' => $item->contentDetails->videoId
      // 'url' => "https://youtube.com/watch?v={$item->contentDetails->videoId}"
    ];
  }, get($playlistUrl)->items);

  //
  // Overwatch League FR Live
  //

  $overwatchLeagueFrChannelId = 'UCI45pR3yMi0uGE47ewT43Ow';
  $overwatchLiveBroadcastUrl = "https://youtube.googleapis.com/youtube/v3/search?part=snippet&channelId={$overwatchLeagueFrChannelId}&eventType=live&maxResults=25&type=video&key={$apiKey}";

  $liveItem = array_map(function($result) {
    return [
      'category' => 'Youtube',
      'name' => "🔴 {$result->snippet->channelTitle}",
      'description' => $result->snippet->title,
      'type' => 'internal',
      'url' => $result->id->videoId
      // 'url' => "https://youtube.com/watch?v={$result->id->videoId}"
    ];
  }, get($overwatchLiveBroadcastUrl)->items);

  //
  // UNION
  //

  return array_merge($liveItem, $playlistItems);
}
