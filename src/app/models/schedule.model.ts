import { JSONObject, required } from "ts-json-object"
import { Media } from "./media.model";
import { Program } from "./program.model";

export class Schedule extends JSONObject {
  @required
  TVA: Program;

  @required
  Noovo: Program;

  @required
  'Radio-Canada': Program;

  @required
  'Tele-Quebec': Program;

  @required
  CBC: Program;

  getProgram(media: Media):  Program | null {
    return this[media.name];
  }
}
