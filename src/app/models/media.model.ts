import {JSONObject, optional, required} from 'ts-json-object'

export enum MediaCategory {
  'TV' = 'TV',
  'Radio'= 'Radio',
//  'Twitch' = 'Twitch',
//  'Youtube' = 'Youtube',
//  'Apps' = 'Apps'
}

export enum MediaType {
  'Internal' = 'internal',
  'External'= 'external',
}

export enum PlayerType {
    'Shaka' = 'shaka',
    'VideoJs' = 'videojs',
    'Native' = 'native'
}

export class Media extends JSONObject {
  @required
  category: MediaCategory

  @required
  name: string

  @optional
  description: string

  @optional
  logo: string

  @required
  type: MediaType;

  @optional
  player: PlayerType;

  @required
  url: string

  get isTv(): boolean {
    return this.category == MediaCategory.TV;
  }

  get isRadio(): boolean {
    return this.category == MediaCategory.Radio;
  }

  get isExternal(): boolean {
    return this.type == MediaType.External;
  }

//  get isYoutube(): boolean {
//    return this.category == MediaCategory.Youtube;
//  }
//
//  get isTwitch(): boolean {
//    return this.category == MediaCategory.Twitch;
//  }

  get isShaka(): boolean {
    return this.player == PlayerType.Shaka;
  }

  get isVideoJs(): boolean {
    return this.player == PlayerType.VideoJs;
  }

  get isNative(): boolean {
    return this.player == PlayerType.Native;
  }

  get playerRoute(): string[] | null {
    if (this.isExternal) {
      return null;
    } else {
      // const nameSegment = this.isYoutube ? this.url : this.name.toLowerCase();
      const nameSegment = this.name.toLowerCase();
      return ['/', this.category.toLowerCase(), nameSegment];
    }
  }

  get source(): { src: string, type?: string } {
    let source : { src: string, type?: string }  = { src: this.url };
      
    if (this.url.endsWith('.mpd') ) {
        source = { ...source, type: 'application/dash+xml' };
    }

    return source;
  }

  ionicon(category): string {
    return `${category.toLowerCase()}-outline`;
//    if (category == MediaCategory.Twitch) {
//      return 'logo-twitch';
//    } else if (category == MediaCategory.Youtube) {
//      return 'logo-youtube';
//    } else {
//      return `${category.toLowerCase()}-outline`;
//    }
  }
}
