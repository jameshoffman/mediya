import { JSONObject, required } from "ts-json-object"

export class Program extends JSONObject {
  @required
  title: string

  @required
  begin: string

  @required
  end: string

  get timeInterval(): string {
    return `${this.begin} - ${this.end}`;
  }
}
