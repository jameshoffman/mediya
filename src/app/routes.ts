import { Routes } from '@angular/router';
import { BrowsePage } from './components/browse/browse.page';
import { PlayerPage } from './components/player/player/player.page';

export const routes: Routes = [
  { path: '', component: BrowsePage },
  { path: ':category/:name', component: PlayerPage },
];
