import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavController, ViewWillEnter } from '@ionic/angular';
import { Media, MediaCategory } from 'src/app/models/media.model';
import { Program } from 'src/app/models/program.model';
import { Schedule } from 'src/app/models/schedule.model';
import { ApiService } from 'src/app/services/api.service';
import { MediasService, MediasSource } from 'src/app/services/medias.service';

@Component({
  selector: 'app-browse',
  templateUrl: './browse.page.html',
  styleUrls: ['./browse.page.scss'],
})
export class BrowsePage implements ViewWillEnter {

  sources : MediasSource[] = [];
  schedule: Schedule | null = null;

  constructor(private title: Title, private mediasService: MediasService, private api: ApiService, private nav: NavController) {
    this.sources = mediasService.allByCategory()
  }

  getProgram(media: Media): (Program)[] {
    const program = this.schedule?.getProgram(media);

    return program ? [ program ] : [];
  }

  ionViewWillEnter() {
    this.api.getTvSchedule().subscribe(res => {
      this.schedule = res;
    });

//    this.api.getTwitchChannels().subscribe(channelsMedia => {
//      this.sources = this.sources.map((source) => {
//        if (source.category == MediaCategory.Twitch) {
//          source.medias = [
//            ...channelsMedia
//          ];
//        }
//
//        return source;
//      });
//    });
//
//    this.api.getYoutubeVideos().subscribe(youtubeMedia => {
//      this.sources = this.sources.map((source) => {
//        if (source.category == MediaCategory.Youtube) {
//          source.medias = [
//            ...youtubeMedia
//          ];
//        }
//
//        return source;
//      });
//    });

    this.title.setTitle('Mediya');
  }

  onCardClicked(media: Media) {
    this.mediasService.open(media, this.nav, 'forward');
  }
}
