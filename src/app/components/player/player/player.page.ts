import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Media, MediaCategory } from 'src/app/models/media.model';
import { MediasService } from 'src/app/services/medias.service';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { PlayerMenuComponent } from '../menu/menu.component';
import VideoJs from 'video.js';

declare let shaka: any;

@Component({
  selector: 'app-player',
  templateUrl: './player.page.html',
  styleUrls: ['./player.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PlayerPage implements AfterViewInit, OnDestroy {
  readonly CATEGORIES = Object.values(MediaCategory);

  @ViewChild('video') video!: ElementRef<HTMLVideoElement>;
  @ViewChild('audio') audio: ElementRef<HTMLAudioElement>;
  @ViewChild('menu') menu: PlayerMenuComponent;

  media: Media = undefined;
  videoJsPlayer = undefined;
  audioFallback: boolean = false;


  @ViewChild('videoPlayer') shakaElement: ElementRef<HTMLVideoElement>;
  @ViewChild('videoContainer') shakaContainer: ElementRef<HTMLDivElement>;
  shakaPlayer = undefined;





  constructor(route: ActivatedRoute, title: Title, mediasService: MediasService, public domSanitizer: DomSanitizer) {
    const name = route.snapshot.paramMap.get('name');

    let youtubeFallbackMedia = new Media({
      category: 'Youtube',
      name: 'Youtube',
      description: '',
      type: 'internal',
      url: `https://www.youtube-nocookie.com/embed/${name}` // for youtube, name is the video id
    });

    this.media = mediasService.findByName(name) ?? youtubeFallbackMedia;

    title.setTitle(`${this.media.name} - Mediya`);
  }

  ngAfterViewInit() {
    this.menu.setCategory(this.media.category, this.media);

    if (this.media.isVideoJs) {
      this.videoJsPlayer = VideoJs(this.video.nativeElement, null, () => {
        this.videoJsPlayer.src(this.media.source);

        this.videoJsPlayer.on('canplay', () => {
          if (this.media.isTv) {
            this.video.nativeElement.style.backgroundImage = 'none';
            this.video.nativeElement.parentElement.style.backgroundImage = 'none';
          }
        });

        this.videoJsPlayer.on('error', () => {
          if (this.media.isRadio) {
            this.audioFallback = true;
          }
        });

        this.videoJsPlayer.play();
      });
    } else if (this.media.isShaka) {
        shaka.polyfill.installAll();

        if (shaka.Player.isBrowserSupported()) {
            this.initPlayer();
        } else {
            console.error('Browser not supported!');
        }
    }
  }

  private initPlayer() {
    this.shakaPlayer = new shaka.Player(this.shakaElement.nativeElement);

    const ui = new shaka.ui.Overlay( this.shakaPlayer, this.shakaContainer.nativeElement, this.shakaElement.nativeElement);

    this.shakaPlayer
      .load(this.media.url)
      .then(() => {
              this.shakaElement?.nativeElement.play();
              })
    .catch((e: any) => {
          console.error(e);
          });
  }

  ngOnDestroy(): void {
    this.videoJsPlayer?.dispose();
  }

  onTabClicked(category: MediaCategory) {
    this.menu.open(category, this.media);
  }
}
