import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Media, MediaCategory } from 'src/app/models/media.model';
import { Program } from 'src/app/models/program.model';
import { Schedule } from 'src/app/models/schedule.model';
import { ApiService } from 'src/app/services/api.service';
import { MediasService } from 'src/app/services/medias.service';

@Component({
  selector: 'app-player-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class PlayerMenuComponent implements OnInit {

  private isSlide: boolean = false;
  currentMedia: Media
  menuCategoryName: string
  mediasForCategory: Media[] = [];
  mediasCategory: MediaCategory = undefined;

  schedule: Schedule | null = null;

  constructor(private mediasService: MediasService, private api: ApiService, private menu: MenuController, private nav: NavController) { }

  ngOnInit() {}

  willOpen() {
    const category = this.isSlide ? this.currentMedia.category : this.mediasCategory;
    this.isSlide = true; // by default, we consider a slide, otherwise flag is set to false in open()

    this.loadMedias(category);
  }

  getProgram(media: Media): (Program)[] {
    const program = this.schedule?.getProgram(media);

    return program ? [ program ] : [];
  }

  setCategory(category: MediaCategory, currentMedia: Media) {
    this.currentMedia = currentMedia;
    this.mediasCategory = category;
  }

  open(category: MediaCategory, currentMedia: Media) {
    this.isSlide = false;

    this.setCategory(category, currentMedia);

    this.loadMedias(category);

    this.menu.open('browse-category-menu');
  }

  onItemClicked(media: Media) {
    this.mediasService.open(media, this.nav, 'root');
  }

  private loadMedias(category: MediaCategory) {
    this.menuCategoryName = category;

    this.mediasForCategory = this.mediasService.findByCategory(category).filter(m => {
      if (this.currentMedia.category == category) {
        return m.name != this.currentMedia.name;
      }

      return true;
    });

    if (category == MediaCategory.TV) {
      this.api.getTvSchedule().subscribe(res => {
        this.schedule = res;
      });
    } 
//    else if (category == MediaCategory.Twitch) {
//      this.api.getTwitchChannels().subscribe(channelsMedia => {
//        this.mediasForCategory = [
//          this.mediasService.TWITCH_MEDIA,
//          ...channelsMedia
//        ];
//      });
//    } else if (category == MediaCategory.Youtube) {
//      this.api.getYoutubeVideos().subscribe(videos => {
//        this.mediasForCategory = [
//          this.mediasService.YOUTUBE_MEDIA,
//          ...videos
//        ];
//      });
//    }
  }
}
