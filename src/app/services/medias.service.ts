import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Media, MediaCategory } from '../models/media.model';

export type MediasSource = { category: MediaCategory, medias: Media[] }

@Injectable({
  providedIn: 'root'
})
export class MediasService {
    readonly TWITCH_MEDIA = new Media({ category: 'Twitch', name: 'Twitch', 
                                        logo: '/assets/logos/twitch.png', type: "external", 
                                        url: 'https://www.twitch.tv/'});
    readonly YOUTUBE_MEDIA = new Media({ category: 'Youtube', name: 'Youtube', 
                                         logo: '/assets/logos/youtube.png', type: "external", 
                                         url: 'https://www.youtube.com/'});

  readonly MEDIAS: Media[] = [
    new Media({ category: 'TV', name: 'TVA', 
                logo: '/assets/logos/tva.png', type: "internal", player: 'videojs',
                url: 'https://tvalive-dai01.akamaized.net/Content/HLS/Live/channel(575b93c5-be31-ee34-6285-14620fd14048)/index.m3u8?__nn__=5529128749001&hdnea=st=1713002400~exp=1713006000~acl=/*~hmac=f13d481a5b7ebb0a5b133075093d5527f757bacb8e5305a17724e5d292d5818a'}),
    new Media({ category: 'TV', name: 'Noovo', 
                logo: '/assets/logos/noovo.png', type: "internal", player: 'shaka',
                url: 'https://pe-ak-lp04a-9c9media.akamaized.net/live/NOOVO/p/dash/00000001/f481c583dbd06b6c/manifest.mpd'}),
    new Media({ category: 'TV', name: 'Radio-Canada', player: 'videojs',
                logo: '/assets/logos/radio-canada.png', type: "internal", 
                url: 'https://rcavlive-dai.akamaized.net/hls/live/696614/cancbftprem/master_5000.m3u8'}),
//    new Media({ category: 'TV', name: 'CBC', 
//                logo: '/assets/logos/cbc.png', type: "internal", player: 'videojs', 
//                url: 'https://cbclivedai3-i.akamaihd.net/hls/live/566976/event2/CBMT/master5.m3u8'}),

    new Media({ category: 'Radio', name: 'Pop Hits', 
                logo: '/assets/logos/pop-hits.webp', type: "internal", player: 'videojs', 
                url: 'https://rfcmedia.streamguys1.com/MusicPulse.mp3'}),
    new Media({ category: 'Radio', name: 'Country Roads', 
                logo: '/assets/logos/country-roads.webp', type: "internal", player: 'videojs', 
                url: 'https://rfcmedia.streamguys1.com/countryroads.mp3'}),
    new Media({ category: 'Radio', name: 'Alt Hits', 
                logo: '/assets/logos/alt-hits.png', type: "internal", player: 'videojs', 
                url: 'https://rfcmedia.streamguys1.com/alternativehits.mp3'}),
    new Media({ category: 'Radio', name: 'Rock On', 
                logo: '/assets/logos/rock-on.png', type: "internal", player: 'videojs', 
                url: 'https://rfcmedia.streamguys1.com/rockon.mp3'}),
    new Media({ category: 'Radio', name: 'z103.5', 
                logo: '/assets/logos/z103.5.png', type: "internal", player: 'native', 
                url: 'https://evanov.leanstream.co/CIDCFM'}),
    new Media({ category: 'Radio', name: 'Ber�euse', 
                logo: '/assets/logos/lullaby.png', type: "internal", player: 'videojs', 
                url: 'https://lullabies.stream.publicradio.org/lullabies.mp3'}),
    new Media({ category: 'Radio', name: '102.3', 
                logo: '/assets/logos/102.3-energie.png', type: "internal", player: 'videojs', 
                url: 'https://playerservices.streamtheworld.com/api/livestream-redirect/CIGBFMAAC.aac'}),
//    new Media({ category: 'Radio', name: 'CHOM', 
//                logo: '/assets/logos/chom.png', type: "internal", player: 'videojs', 
//                url: 'https://playerservices.streamtheworld.com/api/livestream-redirect/CHOMFMAAC.aac'}),
    new Media({ category: 'Radio', name: '106.9', 
                logo: '/assets/logos/106.9-mauricie.png', type: "internal", player: 'videojs', 
                url: 'https://playerservices.streamtheworld.com/api/livestream-redirect/CKOBFMAAC.aac'}),

//    this.TWITCH_MEDIA,
//    this.YOUTUBE_MEDIA,
//
//    new Media({ category: 'Apps', name: 'Prime Music', 
//                logo: '/assets/logos/amazon-music.png', type: "external", 
//                url: 'https://music.amazon.com/'}),
//    new Media({ category: 'Apps', name: 'Prime Video', 
//                logo: '/assets/logos/prime-video.png', type: "external", 
//                url: 'https://www.primevideo.com/storefront/home/'}),
//    new Media({ category: 'Apps', name: 'Optik', 
//                logo: '/assets/logos/optik.png', type: "external", 
//                url: 'https://onthego.telus.com/'})
  ];

  constructor() { }

  allByCategory(): MediasSource[] {
    return Object.values(MediaCategory).map((category: MediaCategory) => {
      return {
        category: category,
        medias: this.findByCategory(category)
      }
    });
  }

  findByCategory(category: MediaCategory): Media[] {
    return this.MEDIAS.filter(m => m.category.toLowerCase()  == category.toLowerCase() );
  }

  findByName(name: string): Media {
    return this.MEDIAS.find(media => media.name.toLowerCase() == name.toLowerCase());
  }

  open(media: Media, nav: NavController, direction: 'forward' | 'backward' | 'root') {
    const playerRoute = media.playerRoute;

    const navigator = {
      'forward': () => {
        nav.navigateForward(playerRoute);
      },
      'backward': () => {
        nav.navigateBack(playerRoute);
      },
      'root': () => {
        nav.navigateRoot(playerRoute);
      },
    };

    if (playerRoute) {
      navigator[direction]();
    } else {
      window.open(media.url, '_self');
    }
  }
}
