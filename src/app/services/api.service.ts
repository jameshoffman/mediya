import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Media } from '../models/media.model';
import { Program } from '../models/program.model';
import { Schedule } from '../models/schedule.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getTvSchedule(): Observable<Schedule> {
    const epochSeconds = `${Math.floor(Date.now() / 1000)}`;

    return this.http.get<any[]>('/api/tv.php', {'params': {epochSeconds}}).pipe(
      map(result => {
        return new Schedule(result.reduce((accumulator, current, i, a) => {
          accumulator[current.name] = new Program(current);

          return accumulator;
        }, {}));
      })
    );
  }

  getTwitchChannels(): Observable<Media[]> {
    return this.http.get<any[]>('/api/twitch.php').pipe(
      map(result => result.map(r => new Media(r)))
    );
  }

  getYoutubeVideos(): Observable<Media[]> {
    return this.http.get<any[]>('/api/youtube.php').pipe(
      map(result => result.map(r => new Media(r)))
    );
  }
}
