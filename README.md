# Mediya

Install Ionic CLI
```bash
npm install -g @ionic/cli
```

## Dev

Start a local php server
```bash
php -S localhost:8088 -t src/api 
```

And the ionic live-reload server
```bash
ionic serve
```

The live-reload server is configured(`angular.json#98` and `src/proxy.conf.json`) to proxy requests to the local php server, in prod the `src/api` folder is copied and Apache serves all content

Ref: https://angular.io/guide/build#proxying-to-a-backend-server

# Prod

The app should be deployed on Apache because `.htaccess` files are used, otherwise it's a pretty minimal LAMP stack(without MySQL). Also install the `php-curl` package.


To build the prod version
```bash
ionic build --prod
```

This generates the `www` folder, which should be served as the Apache Document Root.

Then, on first deploy, create specific `env` file:
```bash
cp www/api/example.env.php www/api/env.php
```

`.gitignore` are used to ignore `env.php` files.
