<?php

header("Content-Type: application/json");

echo json_encode(zap2it($_GET['epochSeconds']));

//
// Functions
//

function formatTime($datetime) {
  $time = substr($datetime, 11, 5);

  $hours = intval(substr($time, 0, 2)) - 4; // time zone conversion
  $hours = ($hours < 0 ? $hours + 24 : $hours);

  $minutes = substr($time, 3, 2);

  return "$hours:$minutes";
}

function zap2it($time) {
  $url = "https://tvlistings.zap2it.com/api/grid?lineupId=CAN-0004921-DEFAULT&timespan=3&headendId=0004921&country=CAN&timezone=&device=X&postalCode=G0X3H0&isOverride=true&time=${time}&languagecode=en";
  $json = json_decode(shell_exec("curl '$url'")); // HACK: Was getting 403 when calling endpoint from curl PHP

  $channelsMap = [
    "2" => "Tele-Quebec",
    "3" => "Radio-Canada",
    "4" => "TVA",
    "5" => "Noovo",
    "6" => "CBC"
  ];

  $requiredChannelsNo = array_keys($channelsMap);
  $channels = array_filter($json->channels, function($channel) use ($requiredChannelsNo) {
    return in_array($channel->channelNo, $requiredChannelsNo);
  });


  $programs = array_map(function($channel) use ($channelsMap) {
    $program = $channel->events[0];

    return [
      'name' => $channelsMap[$channel->channelNo],
      'title' => $program->program->title,
      'begin' => formatTime($program->startTime),
      'end' => formatTime($program->endTime)
    ];
  }, $channels);

  return array_values($programs);
}
