<?php

//
// Init
//

$cURL = curl_init();

//
// Response
//

header("Content-Type: application/json");

echo json_encode(twitch());

//
// Clean up
//

curl_close($cURL);

//
// Functions
//

function get($url, $headers = []) {
  return http($url, FALSE, $headers);
}

function post($url, $headers = []) {
  return http($url, TRUE, $headers);
}

function http($url, $isPost = FALSE, $headers = []) {
  global $cURL;

  curl_setopt($cURL, CURLOPT_URL, $url);
  curl_setopt($cURL, CURLOPT_POST, $isPost);
  curl_setopt($cURL, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($cURL, CURLOPT_RETURNTRANSFER, 1);

  return curl_exec($cURL);
}

function twitch() {

  $env = include('env.php');
  $clientId = $env['TWITCH_CLIENT_ID'];
  $clientSecret = $env['TWITCH_CLIENT_SECRET'];

  $appTokenFile = dirname(__FILE__) . '/twitch_app_token.txt';

  if(!file_exists($appTokenFile)) {
    touch($appTokenFile);
  }
  $appToken = preg_replace( "/\r|\n/", "", file_get_contents($appTokenFile));

  // Find user_id for streamers
  // curl -X GET 'https://api.twitch.tv/helix/users?login=epicjoystick' \
  // -H 'Authorization: Bearer 6eykr3fu9bi32zl1k9rkcsmvkhhzqv' \
  // -H 'Client-Id: is6ssqwv7etcpiwp4f7zxmzq1c7d8z'

  $streamers = [
    '70019920',  // epicjoystick
    '104573615', // madamezoum
    '13822318',  // viictiim
    '125545131', // marypou
    '203850656', // sirene
    '118385536', // achillefps
    '539703465', // nobuspartan
    '81061920',  // 1up
    '614718025', // mathpellerin
    '544571643', // mimi
    '507354293', // chloee_fitgamer
    '40666539',  // nevroze
  ];

  // Try to fetch streams
  $fetchStreams = function($token) use ($streamers, $clientId) {

    $streamersUsersIds = array_reduce($streamers, function($accumulator, $streamer) {
      if ($accumulator) {
        $accumulator .= "&";
      }

      $accumulator .= "user_id=$streamer";

      return $accumulator;
    }, "");
    $headers = array("Authorization: Bearer $token", "Client-Id: $clientId");

    return json_decode(get("https://api.twitch.tv/helix/streams?$streamersUsersIds", $headers));
  };

  $streams = $fetchStreams($appToken);

  if (property_exists($streams, 'status') && $streams->status == 401) {
    $appTokenRefresh = json_decode(post("https://id.twitch.tv/oauth2/token?client_id=${clientId}&client_secret=${clientSecret}&grant_type=client_credentials"));

    $newAppToken = $appTokenRefresh->access_token;
    file_put_contents($appTokenFile, $newAppToken);

    $streams = $fetchStreams($newAppToken);
  }

  $streams = $streams->data;

  $liveStreams = [];

  foreach ($streamers as $streamer) {
    $currentLiveStream = array_filter($streams, function($stream) use ($streamer) {
      return $stream->user_id == $streamer;
    });

    $liveStream = reset($currentLiveStream);

    if ($liveStream) {
      array_push($liveStreams, [
        'category' => 'Twitch',
        'name' => $liveStream->user_name,
        'description' => "{$liveStream->game_name} => {$liveStream->title}",
        'type' => 'external',
        'url' => "https://www.twitch.tv/{$liveStream->user_login}"
      ]);
    }
  }

  return $liveStreams;
}
